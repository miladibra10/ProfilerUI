<%@ page import="model.LogManager" %>
<%@ page import="model.Log" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="model.ColorManager" %><%--
  Created by IntelliJ IDEA.
  User: miladibra
  Date: 2/4/18
  Time: 1:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<title>Elmogame Profiler</title>
<%
    if(!request.getSession().getAttribute("login").equals("login-karde") || request.getSession().getAttribute("login")==null)
    {
        response.sendRedirect("/");
    }
    LogManager logManager = new LogManager();
    ArrayList<String> colors = ColorManager.getColors();
    Random random = new Random();
    HashMap<String,Integer> result = (HashMap<String, Integer>) request.getAttribute("result");
%>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="http://www.chartjs.org/dist/2.7.1/Chart.bundle.js"></script>
<script src="http://www.chartjs.org/samples/latest/utils.js"></script>

<style>
    html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
    <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
    <span class="w3-bar-item w3-right">Elmogame Profiler</span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
    <div class="w3-container w3-row">
        <div class="w3-col s4">
        </div>
        <div class="w3-col s8 w3-bar">
            <span>Welcome to Elmogame Profiler!</span><br>

        </div>
    </div>
    <hr>
    <div class="w3-container">
        <h5>Dashboard</h5>
    </div>
    <div class="w3-bar-block">
        <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
        <a href="/" class="w3-bar-item w3-button w3-padding "><i class="fa fa-bank fa-fw"></i>Home</a>
        <a href="/logout" class="w3-bar-item w3-button w3-padding "><i class="fa fa-close fa-fw"></i>Logout</a>
    </div>

</nav>



<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

    <!-- Header -->
    <header class="w3-container" style="padding-top:22px">
        <h5><b><i class="fa fa-dashboard"></i> My Dashboard</b></h5>
    </header>

    <div class="w3-row-padding w3-margin-bottom">
        <div class="w3-quarter">
            <div class="w3-container w3-<%=colors.get(random.nextInt(colors.size()))%> w3-padding-16">
                <div class="w3-left"><i class="fa fa-eye w3-xxxlarge"></i></div>
                <div class="w3-right">
                    <h3><%=logManager.getLogsNumber()%></h3>
                </div>
                <div class="w3-clear"></div>
                <h4>All Requests</h4>
            </div>
        </div>
        <div class="w3-quarter">
            <div class="w3-container w3-<%=colors.get(random.nextInt(colors.size()))%> w3-padding-16">
                <div class="w3-left"><i class="fa fa-comment w3-xxxlarge"></i></div>
                <div class="w3-right">
                    <h3><%=logManager.getAppsNumber()%></h3>
                </div>
                <div class="w3-clear"></div>
                <h4>Apps</h4>
            </div>
        </div>
        <div class="w3-quarter">
            <div class="w3-container w3-<%=colors.get(random.nextInt(colors.size()))%> w3-padding-16">
                <div class="w3-left"><i class="fa fa-share-alt w3-xxxlarge"></i></div>
                <div class="w3-right">
                    <h3><%=logManager.getURLsNumber()%></h3>
                </div>
                <div class="w3-clear"></div>
                <h4>All URLs</h4>
            </div>
        </div>
        <div class="w3-quarter">
            <div class="w3-container w3-<%=colors.get(random.nextInt(colors.size()))%> w3-text-white w3-padding-16">
                <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
                <div class="w3-right">
                    <h3><%=logManager.getIPsNumber()%></h3>
                </div>
                <div class="w3-clear"></div>
                <h4>All IPs</h4>
            </div>
        </div>
    </div>

    <div class="w3-panel">
        <canvas id="canvas"></canvas>
    </div>

    <!-- Footer -->
    <footer class="w3-container w3-padding-16 w3-light-grey">
        <h4>ELMOGAME PROFILER</h4>
        <p>Powered by <a href="https://www.elmogame.com" target="_blank">elmogame.com</a></p>
    </footer>

    <!-- End page content -->
</div>

<script>
    // Get the Sidebar
    var mySidebar = document.getElementById("mySidebar");

    // Get the DIV with overlay effect
    var overlayBg = document.getElementById("myOverlay");

    // Toggle between showing and hiding the sidebar, and add overlay effect
    function w3_open() {
        if (mySidebar.style.display === 'block') {
            mySidebar.style.display = 'none';
            overlayBg.style.display = "none";
        } else {
            mySidebar.style.display = 'block';
            overlayBg.style.display = "block";
        }
    }

    // Close the sidebar with the close button
    function w3_close() {
        mySidebar.style.display = "none";
        overlayBg.style.display = "none";
    }
</script>
<script>
    var MONTHS = [<%
    for (String url : result.keySet()) {
      out.print("\""+url+"\",");
    }
    %>];
    var color = Chart.helpers.color;
    var barChartData = {
        labels: [<%
    for (String url : result.keySet()) {
      out.print("\""+url+"\",");
    }
    %>],
        datasets: [{
            label: '<%=request.getParameter("app")%>',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            borderWidth: 1,
            data: [
                <%
                    for (Integer number : result.values()) {
                      out.print(number+",");
                    }
                %>
            ]
        }]

    };

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Chart.js Bar Chart'
                }
            }
        });

    };
</script>

</body>
</html>
