package controller;

import model.UserManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginController extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("user");
        String password = request.getParameter("pass");
        UserManager userManager = new UserManager();
        if(userManager.doesUserExist(userName,password))
        {
            request.getSession().setAttribute("login","login-karde");
        }
        response.sendRedirect("/");
    }
}
