package controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Index extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute("login")!=null && request.getSession().getAttribute("login").equals("login-karde"))
        {
            request.getRequestDispatcher("/home.jsp").forward(request,response);
        }
        else
        {
            request.getRequestDispatcher("/login.jsp").forward(request,response);
        }
    }
}
