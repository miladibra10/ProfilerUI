package controller;

import model.LogManager;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UrlController extends HttpServlet {
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute("login")!=null && request.getSession().getAttribute("login").equals("login-karde"))
        {
            LogManager logManager = new LogManager();
            request.setAttribute("app",request.getParameter("app"));
            request.setAttribute("result",logManager.getAppURLsDistribution(request.getParameter("app")));
            request.getRequestDispatcher("/urls.jsp").forward(request,response);
        }
        else
        {
            response.sendRedirect("/");
        }
    }
}
