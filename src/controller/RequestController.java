package controller;

import model.Configuration;
import model.Log;
import model.LogManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class RequestController extends HttpServlet{
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute("login")!=null && request.getSession().getAttribute("login").equals("login-karde") )
        {
            LogManager logManager = new LogManager();
            if(request.getParameter("app").equals("all"))
            {
                request.setAttribute("all",logManager.getLogsNumber()/ Configuration.getAppLimit()+1);
            }
            else
            {
                request.setAttribute("all",logManager.getAppLogsNumber(request.getParameter("app"))/Configuration.getAppLimit()+1);
            }
            ArrayList<Log> result;
            if(request.getParameter("app").equals("all"))
            {
                result = logManager.getAllLogs(Configuration.getAppLimit(),Integer.parseInt(request.getParameter("page")));
            }
            else
            {
                result = logManager.getAppLogs(request.getParameter("app"),Configuration.getAppLimit(),Integer.parseInt(request.getParameter("page")));
            }
            request.setAttribute("result",result);
            request.getRequestDispatcher("/requests.jsp").forward(request,response);
        }
        else
        {
            response.sendRedirect("/");
        }
    }
}
