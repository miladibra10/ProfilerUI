package model;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;

import java.sql.DriverManager;
import java.sql.SQLException;

public class UserDA {
    private Connection connection;
    public UserDA() {
        try {
            Class.forName(Configuration.getMysqlDriver());
            connection = (Connection) DriverManager.getConnection(Configuration.getConnectionString(), Configuration.getMysqlUser(), Configuration.getMysqlPass());
            connection.setAutoCommit(false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean doesUserExist(String userName,String passWord) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * from Users WHERE Username=? AND Password=?");
        preparedStatement.setString(1,userName);
        preparedStatement.setString(2,passWord);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        if(resultSet.next())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
