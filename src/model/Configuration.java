package model;

public class Configuration {
    private static final String mysqlDriver = "com.mysql.jdbc.Driver";
    private static final String mysqlUser = "profiler";
    private static final String mysqlPass = "est946p1376104";
    private static final String connectionStr = "jdbc:mysql://localhost/";
    private static final String dataBaseName = "test";
    private static final int appLimit = 25 ;
    private static final int indexLimit = 10 ;

    public static int getAppLimit() {
        return appLimit;
    }

    public static int getIndexLimit() {
        return indexLimit;
    }

    public static String getDataBaseName() {
        return dataBaseName;
    }

    public static String getMysqlDriver() {
        return mysqlDriver;
    }

    public static String getConnectionString() {
        return connectionStr+getDataBaseName();
    }

    public static String getMysqlUser() {
        return mysqlUser;
    }

    public static String getMysqlPass() {
        return mysqlPass;
    }
}
