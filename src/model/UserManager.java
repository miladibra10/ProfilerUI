package model;

public class UserManager {
    private UserDA userDA;
    public UserManager()
    {
        userDA = new UserDA();
    }
    public boolean doesUserExist(String userName,String rawPassword)
    {
        try {
            String hashPassword = EncoderUtil.getMD5(rawPassword);
            return userDA.doesUserExist(userName,hashPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
