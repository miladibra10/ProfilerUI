package model;

import java.util.ArrayList;

public class ColorManager {
    public static ArrayList<String> getColors ()
    {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("green");
        colors.add("orange");
        colors.add("aqua");
        colors.add("blue");
        colors.add("lime");
        colors.add("pink");
        colors.add("yellow");
        colors.add("red");
        colors.add("teal");
        return colors;
    }
}
