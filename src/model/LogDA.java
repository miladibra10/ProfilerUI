package model;

import com.mysql.jdbc.*;
import javafx.util.Pair;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class LogDA {
    private Connection connection;
    public LogDA() {
        try {
            Class.forName(Configuration.getMysqlDriver());
            connection = (Connection) DriverManager.getConnection(Configuration.getConnectionString(), Configuration.getMysqlUser(), Configuration.getMysqlPass());
            connection.setAutoCommit(false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Log> getLimitedLogs(String condition ,int limit , int pageNumber) {
        ArrayList<Log> result = new ArrayList<>();
        if(condition.equals(""))
        {
            condition="1=1";
        }
        try {
            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM Requests WHERE "+condition+" ORDER BY Request_ID DESC limit ?,?");
            preparedStatement.setInt(1, (pageNumber - 1) * limit);
            preparedStatement.setInt(2, limit);
            ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
            while (resultSet.next()) {
                Log temp = new Log();
                temp.setRemoteAddress(getIP(resultSet.getInt("IP_ID")));
                temp.setRequestBody(resultSet.getString("RequestBody"));
                temp.setBytesSent(resultSet.getInt("BytesSent"));
                temp.setRequestTime(resultSet.getFloat("RequestTime"));
                temp.setStatus(resultSet.getInt("Status"));
                ///
                Request req = new Request();
                req.setRequestType(getRequestType(resultSet.getInt("Type_ID")));
                req.setRequestProtocol(getProtocol(resultSet.getInt("Protocol_ID")));
                req.setRequestAddress(getURL(resultSet.getInt("URL_ID")));
                temp.setRequest(req);
                ///
                Pair<String, String> app = getAppData(resultSet.getInt("App_ID"));
                temp.setAppName(app.getKey());
                temp.setPort(app.getValue());
                result.add(temp);
            }
            return result;
        }catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Log> getAppLogs(String appName , int limit , int pageNumber) {
        try {
            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT App_ID from Apps WHERE AppName=?");
            preparedStatement.setString(1,appName);
            ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
            int appID=0;
            while (resultSet.next())
            {
                appID=resultSet.getInt("App_ID");
            }
            String condition = "App_ID="+appID;
            return getLimitedLogs(condition,limit,pageNumber);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getLogsNumber() throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT COUNT(*) FROM Requests");
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next()) {
            return resultSet.getInt("COUNT(*)");
        }
        return 0; ///IF EMPTY
    }

    public int getIPsNumber() throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT COUNT(*) FROM IPs");
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next()) {
            return resultSet.getInt("COUNT(*)");
        }
        return 0; ///IF EMPTY
    }

    public int getAppsNumber() throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT COUNT(*) FROM Apps");
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next()) {
            return resultSet.getInt("COUNT(*)");
        }
        return 0; ///IF EMPTY
    }

    public int getURLsNumber() throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT COUNT(*) FROM URLs");
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next()) {
            return resultSet.getInt("COUNT(*)");
        }
        return 0; ///IF EMPTY
    }

    public int getAPPURLNumber(int URL_ID,int App_ID) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT COUNT(*) FROM Requests WHERE URL_ID=? and App_ID=?");
        preparedStatement.setInt(1,URL_ID);
        preparedStatement.setInt(2,App_ID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next()) {
            return resultSet.getInt("COUNT(*)");
        }
        return 0;
    }

    public int getAppLogsNumber(int App_ID) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT COUNT(*) FROM Requests WHERE App_ID=?");
        preparedStatement.setInt(1,App_ID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next()) {
            return resultSet.getInt("COUNT(*)");
        }
        return 0;
    }

    public int getAppID(String appName) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT App_ID FROM Apps WHERE AppName=?");
        preparedStatement.setString(1,appName);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next())
        {
            return resultSet.getInt("App_ID");
        }
        return 0;
    }

    public String getIP(int IP_ID) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM IPs WHERE IP_ID=?");
        preparedStatement.setInt(1,IP_ID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getString("IPAddress");
    }

    public String getRequestType(int Type_ID) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM RequestTypes WHERE Type_ID=?");
        preparedStatement.setInt(1,Type_ID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getString("RequestType");
    }

    public String getURL(int URL_ID) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM URLs WHERE URL_ID=?");
        preparedStatement.setInt(1,URL_ID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getString("URL");
    }

    public String getProtocol(int Protocol_ID) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM Protocols WHERE Protocol_ID=?");
        preparedStatement.setInt(1,Protocol_ID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getString("Protocol");
    }

    public Pair<String,String> getAppData(int App_ID) throws SQLException {
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM Apps WHERE App_ID=?");
        preparedStatement.setInt(1,App_ID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        resultSet.next();
        String appName = resultSet.getString("AppName");
        String port = resultSet.getString("Port");
        Pair<String,String> result = new Pair<>(appName,port);
        return result;
    }

    public HashMap<String,Integer> getAppsData() throws SQLException {
        HashMap<String,Integer> result = new HashMap<>();
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("" +
                "SELECT * FROM Apps");
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while (resultSet.next())
        {
            result.put(resultSet.getString("AppName"),resultSet.getInt("App_ID"));
        }
        return result;
    }

    public HashMap<String,Integer> getAppURLDistributions(int appID) throws SQLException {
        HashMap<String,Integer> result = new HashMap<>();
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT DISTINCT URL_ID FROM Requests WHERE App_ID=?");
        preparedStatement.setInt(1,appID);
        ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();
        while(resultSet.next())
        {
            result.put(getURL(resultSet.getInt("URL_ID")),getAPPURLNumber(resultSet.getInt("URL_ID"),appID));
        }
        return result;
    }



}
