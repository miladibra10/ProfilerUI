package model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class LogManager {
    LogDA logDa;

    public LogManager() {
        logDa = new LogDA();
    }

    public ArrayList<Log> getAllLogs(int limit, int number) {
        return logDa.getLimitedLogs("", limit, number);
    }

    public ArrayList<Log> getAppLogs(String appName, int limit, int number) {
        return logDa.getAppLogs(appName, limit, number);
    }

    public HashMap<String,Float> getAppDistribution() {
        HashMap<String,Float> distribution = new HashMap<>();
        try {
            int allLogs = logDa.getLogsNumber();
            HashMap<String,Integer> apps = logDa.getAppsData();
            for (String appName : apps.keySet()) {
                int appRequestNumbers = logDa.getAppLogsNumber(apps.getOrDefault(appName,0));
                Float percentage = new Float(appRequestNumbers/allLogs*100);
                distribution.put(appName,percentage);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return distribution;
    }

    public int getLogsNumber() {
        try {
            return logDa.getLogsNumber();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getIPsNumber() {
        try {
            return logDa.getIPsNumber();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getAppsNumber() {
        try {
            return logDa.getAppsNumber();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getURLsNumber() {
        try {
            return logDa.getURLsNumber();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getNextPage(int current , int all,String app) {
        if(current==all)
        {
            return "#";
        }
        else
        {
            return "/requests?page="+String.valueOf(current+1)+"&app="+app;
        }
    }

    public String getPrevPage(int current , int all , String app) {
        if(current==1)
        {
            return "#";
        }
        else
        {
            return "/requests?page="+String.valueOf(current-1)+"&app="+app;
        }
    }

    public int getAppLogsNumber(String appName) {
        try {
            int appID=logDa.getAppID(appName);
            return logDa.getAppLogsNumber(appID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public HashMap<String,Integer> getAppURLsDistribution(String appName)
    {
        try {
            int appID = logDa.getAppID(appName);
            return logDa.getAppURLDistributions(appID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
